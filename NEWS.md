﻿# jab.adverse.reactions.full 1.0.2 (13 March 2022)

* Removed the | file LICENSE statement from the DESCRIPTION file as it was not needed (and incorrect -- CCO)



# jab.adverse.reactions.full 1.0.0 (13 January 2022)

* Initial release {This full version of the `jab.adverse.reactions` package was created due to the increasing size of the pdf and the public subfolders of the inst directory in the original package}
